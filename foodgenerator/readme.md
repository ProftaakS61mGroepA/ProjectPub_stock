## Food Generator script

A small python script to randomly generated food-themed products, and insert them into Stock.

### How to install

- Install Node.js (make sure it's added to path)
- Install Python 3.6, (make sure it's added to path)
- Install dependencies: `pip install -r requirements.txt`

### How to use

- Open a cmd window in the upper `foodgenerator` directory (containing `foodgenerator` and `tests` directories)
- Call `python -m foodgenerator --help` to see available arguments