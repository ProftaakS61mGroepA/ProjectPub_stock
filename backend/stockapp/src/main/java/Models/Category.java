package Models;

public enum Category {
    FOOD,
    DRINK,
    OTHER
}
