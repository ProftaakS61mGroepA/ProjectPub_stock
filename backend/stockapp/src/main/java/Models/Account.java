package Models;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@NamedQueries(value = {
        @NamedQuery(name = "accountdao.findByGoogleId", query = "SELECT p FROM Account p where p.googleId = :googleId")
})
public class Account extends ModelBase {

    @Column(nullable = false)
    private String googleId;

    public Account() { }

    public Account(String googleId) {
        this.googleId = googleId;
    }

    public String getGoogleId() {
        return googleId;
    }

    public void setGoogleId(String googleId) {
        this.googleId = googleId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        return googleId != null ? googleId.equals(account.googleId) : account.googleId == null;
    }

    @Override
    public int hashCode() {
        return googleId != null ? googleId.hashCode() : 0;
    }
}
