package Models;

import DataTransferObject.JsonProduct;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.util.*;

@Entity
@XmlRootElement
public class Product extends ModelBase {

    @Column(nullable = false, unique = true)
    private String name;
    private long priceInCents;
    @Enumerated(EnumType.STRING)
    private Category category;
    private long inStock = 5;
    private long reserved = 0;

    public Product() { }

    public Product(String name, long priceInCents, Category category) {
        this.name = name;
        this.priceInCents = priceInCents;
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPriceInCents() {
        return priceInCents;
    }

    public void setPriceInCents(long priceInCents) {
        this.priceInCents = priceInCents;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public long getInStock() {
        return inStock;
    }

    public void setInStock(long inStock) {
        this.inStock = inStock;
    }

    public long getAvailable() {
        return inStock - reserved;
    }

    //TODO: exception voor invalid amount
    public void use(int amount) {
        setReserved(getReserved() - amount);
        setInStock(getInStock() - amount);
    }

    public long getReserved() {
        return reserved;
    }

    public void setReserved(long reserved) {
        this.reserved = reserved;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (priceInCents != product.priceInCents) return false;
        if (inStock != product.inStock) return false;
        if (name != null ? !name.equals(product.name) : product.name != null) return false;
        return category == product.category;
    }

    public static Product fromJsonProduct(JsonProduct jsonProduct) {
        Product product = new Product(jsonProduct.getName(),
                jsonProduct.getPriceInCents(),
                jsonProduct.getCategory());
        product.setId(jsonProduct.getId());
        return product;
    }

    public static List<Product> fromProducts(List<JsonProduct> jsonProducts) {
        List<Product> products = new ArrayList<>();
        for (JsonProduct jsonProduct : jsonProducts) {
            products.add(fromJsonProduct(jsonProduct));
        }
        return products;
    }
}
