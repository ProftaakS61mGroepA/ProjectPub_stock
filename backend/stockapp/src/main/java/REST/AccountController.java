package REST;

import DataTransferObject.*;
import Service.AccountService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

@Stateless
@Path("/accounts")
public class AccountController {

    @Inject
    private AccountService accountService;

    @POST
    @Path("/login")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String requestLogin(String redirectUrl) {
        return accountService.requestLogin(redirectUrl).toString();
    }

    @POST
    @Path("/postlogin")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public JsonAccount accountLoggedin(AuthorizationInfo authorizationInfo) {
        JsonAccount account = JsonAccount.fromAccount(accountService.accountAuthorized(authorizationInfo.getAuthorizationCode(), authorizationInfo.getRedirectUrl()));
        account.setAccessToken(accountService.getAccessToken(account.getId()));
        return account;
    }
}
