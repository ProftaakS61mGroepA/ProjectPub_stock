package REST;

import WebSocket.WebSocketEndPoint;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Set;

@ApplicationPath("/*")
public class Stockapp extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(REST.AccountController.class);
        resources.add(REST.CORSFilter.class);
        resources.add(REST.ProductController.class);
    }

}
