package REST;

import DataTransferObject.JsonProduct;
import Models.Product;
import Service.ProductService;

import javax.annotation.security.PermitAll;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.List;
import com.google.gson.Gson;

@Stateless
@Path("/products")
@PermitAll
public class ProductController {

    @Inject
    private ProductService productService;

    @Context
    private UriInfo uriInfo;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(JsonProduct product) {
        final JsonProduct newProduct = JsonProduct.fromProduct(productService.create(Product.fromJsonProduct(product)));
        final URI uri = uriInfo.getAbsolutePathBuilder().path(newProduct.getId() + "").build();
        return Response.created(uri).entity(newProduct).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<JsonProduct> getAll() {
        return JsonProduct.fromProducts(productService.getAll());
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public JsonProduct findById(@PathParam("id") long productId) {
        return JsonProduct.fromProduct(productService.findById(productId));
    }

    @DELETE
    @Path("/{id}")
    public Response remove(@PathParam("id") long productId) {
        productService.removeProduct(productId);
        return Response.accepted().build();
    }

    @POST
    @Path("/updated")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<JsonProduct> getUpdatedProducts(List<JsonProduct> products) {
        return JsonProduct.fromProducts(productService.getUpdatedProducts(Product.fromProducts(products)));
    }

    @PUT
    @Path("/reserve")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<JsonProduct> reserveProducts(List<JsonProduct> products) {
        return JsonProduct.fromProducts(productService.reserveProducts(Product.fromProducts(products)));
    }

    @PUT
    @Path("/use")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<JsonProduct> useProducts(List<JsonProduct> products) {
        return JsonProduct.fromProducts(productService.useProducts(Product.fromProducts(products)));
    }

    @PUT
    @Path("/order")
    @Consumes(MediaType.APPLICATION_JSON)
    public void orderProduct(String json) {
        final long[] orderInfo = new Gson().fromJson(json, long[].class);
        productService.orderProduct(orderInfo[0], orderInfo[1]);
    }
}
