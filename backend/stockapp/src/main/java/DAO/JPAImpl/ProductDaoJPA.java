package DAO.JPAImpl;

import DAO.IProductDao;
import Models.Product;

import javax.ejb.Stateless;
import javax.enterprise.inject.Default;
import javax.persistence.*;
import java.util.List;

@Stateless
@Default
public class ProductDaoJPA implements IProductDao {

    @PersistenceContext(unitName = "ProjectPub_StockPU")
    private EntityManager em;

    public ProductDaoJPA() { }

    @Override
    public List<Product> getAll() {
        return em.createQuery("select t from " + Product.class.getSimpleName() + " t").getResultList();
    }

    @Override
    public Product findById(Object id) {
        return em.find(Product.class, id);
    }

    @Override
    public Product create(Product product) {
        if (product.getName().trim().equals("") ||
                product.getPriceInCents() < 0 ||
                product.getCategory() == null) {
            throw new NullPointerException();
        }
        em.persist(product);
        return product;
    }

    @Override
    public Product edit(Product entity) {
        //ToDo User Story: Modify existing product
        //https://trello.com/c/8sCvsiUu/26-3-modify-existing-product
        return null;
    }

    @Override
    public void remove(Product entity) {
        em.remove(em.merge(entity));
    }
}
