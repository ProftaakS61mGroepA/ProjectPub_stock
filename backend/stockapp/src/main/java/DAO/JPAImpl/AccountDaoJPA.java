package DAO.JPAImpl;

import DAO.IAccountDao;
import Models.Account;

import javax.ejb.Stateless;
import javax.enterprise.inject.Default;
import javax.persistence.*;
import java.util.List;

@Stateless
@Default
public class AccountDaoJPA implements IAccountDao {

    @PersistenceContext
    private EntityManager em;

    public AccountDaoJPA() { }

    @Override
    public List<Account> getAll() {
        return em.createQuery("select t from " + Account.class.getSimpleName() + " t").getResultList();
    }

    @Override
    public Account findById(Object id) {
        return em.find(Account.class, id);
    }

    @Override
    public Account create(Account account) {
        if (account.getGoogleId().equals("")) {
            throw new NullPointerException();
        }
        em.persist(account);
        return account;
    }

    @Override
    public Account edit(Account entity) {
        return em.merge(entity);
    }

    @Override
    public Account findByGoogleId(String googleId) {
        Query q = em.createNamedQuery("accountdao.findByGoogleId");
        q.setParameter("googleId", googleId);

        Account account;
        try {
            account = (Account)q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
        return account;
    }
}
