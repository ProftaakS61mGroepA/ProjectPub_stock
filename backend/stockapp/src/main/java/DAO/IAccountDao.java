package DAO;

import Models.Account;

public interface IAccountDao extends IDaoBase<Account> {

    Account findByGoogleId(String googleId);
}
