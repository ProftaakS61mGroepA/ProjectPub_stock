package DAO;

import java.util.List;

public interface IDaoBase<T> {
    List<T> getAll();
    T findById(Object id);
    T create(T entity);
    T edit(T entity);
}
