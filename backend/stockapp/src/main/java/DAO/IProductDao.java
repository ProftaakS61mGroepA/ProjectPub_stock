package DAO;

import Models.Product;

public interface IProductDao extends IDaoBase<Product> {

    void remove(Product product);
}
