package DataTransferObject;

import Models.Account;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class JsonAccount {

    private long id;
    private String googleId;
    private String accessToken;

    public JsonAccount() { }

    public JsonAccount(long id, String googleId) {
        this.id = id;
        this.googleId = googleId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGoogleId() {
        return googleId;
    }

    public void setGoogleId(String googleId) {
        this.googleId = googleId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JsonAccount that = (JsonAccount) o;

        if (id != that.id) return false;
        if (googleId != null ? !googleId.equals(that.googleId) : that.googleId != null) return false;
        return accessToken != null ? accessToken.equals(that.accessToken) : that.accessToken == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (googleId != null ? googleId.hashCode() : 0);
        result = 31 * result + (accessToken != null ? accessToken.hashCode() : 0);
        return result;
    }

    public static JsonAccount fromAccount(Account account) {
        return new JsonAccount(account.getId(), account.getGoogleId());
    }
}
