package DataTransferObject;

import com.google.gson.Gson;

public class JMSMessage {

    private long id;
    private long productId;
    private String productName;
    private long amount;

    public JMSMessage(long id, long productId, String productName, long amount) {
        this.id = id;
        this.productId = productId;
        this.productName = productName;
        this.amount = amount;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public String toJson() {
        return new Gson().toJson(this);
    }

    public static JMSMessage fromJson(String json) {
        return new Gson().fromJson(json, JMSMessage.class);
    }
}
