package DataTransferObject;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AuthorizationInfo {
    private String authorizationCode;
    private String redirectUrl;

    public AuthorizationInfo() { }

    public AuthorizationInfo(String authorizationCode, String redirectUrl) {
        this.authorizationCode = authorizationCode;
        this.redirectUrl = redirectUrl;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }
}
