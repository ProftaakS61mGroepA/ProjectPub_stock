package DataTransferObject;

import Models.*;
import com.google.gson.Gson;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.*;

@XmlRootElement
public class JsonProduct {
    private long id;
    private String name;
    private long priceInCents;
    private Category category;
    private long available;

    public JsonProduct() { }

    public JsonProduct(long id, String name, long priceInCents, Category category, long available) {
        this.id = id;
        this.name = name;
        this.priceInCents = priceInCents;
        this.category = category;
        this.available = available;
    }

    public JsonProduct(String name, long priceInCents, Category category) {
        this.name = name;
        this.priceInCents = priceInCents;
        this.category = category;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPriceInCents() {
        return priceInCents;
    }

    public void setPriceInCents(long priceInCents) {
        this.priceInCents = priceInCents;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public long getAvailable() {
        return available;
    }

    public void setAvailable(long available) {
        this.available = available;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JsonProduct that = (JsonProduct) o;

        if (id != that.id) return false;
        if (priceInCents != that.priceInCents) return false;
        if (available != that.available) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return category == that.category;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (int) (priceInCents ^ (priceInCents >>> 32));
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (int) (available ^ (available >>> 32));
        return result;
    }

    public static JsonProduct fromProduct(Product product) {
        return new JsonProduct(product.getId(),
                product.getName(),
                product.getPriceInCents(),
                product.getCategory(),
                product.getAvailable());
    }

    public static List<JsonProduct> fromProducts(List<Product> products) {
        List<JsonProduct> jsonProducts = new ArrayList<>();
        for (Product product : products) {
            jsonProducts.add(fromProduct(product));
        }
        return jsonProducts;
    }

    public String toJson() {
        return new Gson().toJson(this);
    }

    public static JsonProduct fromJson(String json) {
        return new Gson().fromJson(json, JsonProduct.class);
    }
}
