package WebSocket;

import DataTransferObject.JsonProduct;
import Models.Product;

import javax.enterprise.context.ApplicationScoped;
import javax.websocket.Session;
import java.io.IOException;
import java.util.*;

@ApplicationScoped
public class WebSocketSessionHandler {

    private static Set<Session> sessions = Collections.synchronizedSet(new HashSet<Session>());

    public void addSession(Session session) {
        sessions.add(session);
    }

    public void removeSession(Session session) {
        try {
            if (session.isOpen()) {
                session.close();
            }
        } catch (IOException ex) { }
        finally {
            sessions.remove(session);
        }
    }

    private Set<Session> getSessions() {
        return new HashSet<>(sessions);
    }

    public void sendToAll(Product product) {
        getSessions().forEach(s -> send(s, JsonProduct.fromProduct(product).toJson()));
    }

    private void send(Session session, String message) {
        session.getAsyncRemote().sendText(message);
    }
}
