package JMS;

import DataTransferObject.JMSMessage;
import Service.ConfigService;
import Service.ProductService;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.jms.*;

@Singleton
@Startup
public class JMSBrokerGateway {

    @Inject
    private ProductService productService;
    @Inject
    private ConfigService configService;

    private MessageSenderGateway sender;
    private MessageReceiverGateway receiver;

    private long id = 0;

    public JMSBrokerGateway() { }

    @PostConstruct
    private void load() {
        try
        {
            String outputQueue = configService.getProperty("jms.broker.outputqueue"),
                inputQueue = configService.getProperty("jms.broker.inputqueue"),
                initialContextFactory = configService.getProperty("jms.broker.contextfactory"),
                providerUrl = configService.getProperty("jms.broker.providerurl");

        sender = new MessageSenderGateway(outputQueue, initialContextFactory, providerUrl);
        receiver = new MessageReceiverGateway(inputQueue, initialContextFactory, providerUrl);
        receiver.setListener(message -> {
            if (message instanceof TextMessage)
            {
                try {
                    String json = ((TextMessage)message).getText();
                    onMessageArrived(json);
                } catch (JMSException e) {
                    e.printStackTrace();
                }
            }
        });
        } catch (Exception e)
        {
            
        }
    }

    private void onMessageArrived(String json) {
        JMSMessage message = JMSMessage.fromJson(json);
        productService.onOrderArrived(message.getProductId(), message.getAmount());
    }

    public void sendMessage(long productId, String productName, long amount) {
        sendMessage(new JMSMessage(nextId(), productId, productName, amount));
    }

    private void sendMessage(JMSMessage message) {
        sender.send(sender.createTextMessage(message.toJson()));
    }

    private long nextId() {
        id = id++;
        return id;
    }

    @PreDestroy
    private void close() {
        sender.close();
        receiver.close();
    }
}
