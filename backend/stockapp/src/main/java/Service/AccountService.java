package Service;

import DAO.IAccountDao;
import DataTransferObject.GoogleUserInfo;
import HttpClient.GoogleController;
import Models.Account;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.IOException;
import java.net.URI;

@Stateless
public class AccountService {

    @Inject
    private IAccountDao accountDao;
    @Inject
    private OAuthService oAuthService;
    @Inject
    private GoogleController googleController;

    public AccountService() { }

    public URI requestLogin(String redirectUrl) {
        return oAuthService.getAuthorizationUri(redirectUrl);
    }

    public Account accountAuthorized(String authorizationCode, String redirectUrl) {
        Account account;
        try {
            GoogleTokenResponse tokenResponse = oAuthService.getAccessToken(authorizationCode, redirectUrl);
            GoogleUserInfo userInfo = googleController.getUserInfo(tokenResponse.getTokenType() + " " + tokenResponse.getAccessToken());
            account = findByGoogleId(userInfo.getId());
            if (account == null) {
                account = create(new Account(userInfo.getId()));
            }
            Object credentials = oAuthService.getCredentials(account.getId() + "");
            if (credentials == null) {
                oAuthService.storeCredentials(tokenResponse, account.getId() + "");
            }
        } catch (IOException e) {
            throw new NullPointerException(e.getMessage());
        }

        return account;
    }

    public Account findByGoogleId(String googleId) {
        return accountDao.findByGoogleId(googleId);
    }

    public Account create(Account account) {
        return accountDao.create(account);
    }

    public String getAccessToken(long userId) {
        try {
            return "Bearer " + oAuthService.getCredentials(userId + "").getAccessToken();
        } catch (IOException e) {
            throw new NullPointerException(e.getMessage());
        }
    }
}
