package Service;

import DAO.IProductDao;
import JMS.JMSBrokerGateway;
import Models.Product;
import WebSocket.WebSocketSessionHandler;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.*;

@Stateless
public class ProductService {

    @Inject
    private IProductDao productDao;
    @Inject
    private JMSBrokerGateway jmsBroker;
    @Inject
    private WebSocketSessionHandler webSocketSessionHandler;

    public Product create(Product product) {
        //Check if a product with the same name already exists
        List<Product> currentProducts = getAll();
        for (Product p : currentProducts ) {
            if (p.getName().equals(product.getName())) {
                return null;
            }
        }

        productDao.create(product);
        return product;
    }

    public List<Product> getAll() {
        return productDao.getAll();
    }

    public Product findById(long id) {
        return productDao.findById(id);
    }

    public void removeProduct(long productId) {
        productDao.remove(findById(productId));
    }

    public List<Product> getUpdatedProducts(List<Product> products) {
        List<Product> requestedProducts = new ArrayList<>();
        for (Product p : products) {
            Product tempProduct = findById(p.getId());
            requestedProducts.add(tempProduct);
        }
        return requestedProducts;
    }

    public List<Product> reserveProducts(List<Product> products) {
        //Update products and check duplicate occurrences
        HashMap<Product, Integer> requestedProducts = getAmountOfRequested(updateProducts(products));

        //Check if the requested products are reservable
        for (Product p : requestedProducts.keySet()) {
            if (p.getAvailable() < requestedProducts.get(p)) {
                throw new NullPointerException(requestedProducts.get(p) + "x " + p.getName() + " is not available.");
            }
        }

        //Reserve and return the requested products
        List<Product> returnProducts = new ArrayList<>();
        for (Product p : requestedProducts.keySet()) {
            p.setReserved(p.getReserved() + requestedProducts.get(p));
            for (int i = 0; i < requestedProducts.get(p); i++) {
                returnProducts.add(p);
            }
            //Save latest state in the database
            productDao.edit(p);
        }
        return returnProducts;
    }

    public List<Product> useProducts(List<Product> products) {
        //Update products and check duplicate occurrences
        HashMap<Product, Integer> requestedProducts = getAmountOfRequested(updateProducts(products));

        //Check if the requested products are reservable
        for (Product p : requestedProducts.keySet()) {
            if (p.getReserved() < requestedProducts.get(p)) {
                throw new NullPointerException(requestedProducts.get(p) + "x " + p.getName() + " is not reserved.");
            }
        }

        //Set used and return the requested products
        List<Product> returnProducts = new ArrayList<>();
        for (Product p : requestedProducts.keySet()) {
            p.use(requestedProducts.get(p));
            for (int i = 0; i < requestedProducts.get(p); i++) {
                returnProducts.add(p);
            }
            //Save latest state in the database
            productDao.edit(p);
        }
        return returnProducts;
    }

    public void orderProduct(long productId, long amount) {
        Product product = productDao.findById(productId);
        jmsBroker.sendMessage(product.getId(), product.getName(), amount);
    }

    public void onOrderArrived(long productId, long amount) {
        Product product = productDao.findById(productId);
        product.setInStock(product.getInStock() + amount);
        productDao.edit(product);
        webSocketSessionHandler.sendToAll(product);
    }

    /**
     * Updates a List of products to the latest state in the database
     */
    private List<Product> updateProducts(List<Product> products) {
        List<Product> updatedProducts = new ArrayList<>();
        for (Product p : products) {
            updatedProducts.add(findById(p.getId()));
        }
        return updatedProducts;
    }

    /**
     * Gets the number of occurrences of a product in the list and returns this in a HashMap.
     */
    private HashMap<Product, Integer> getAmountOfRequested(List<Product> requestedProducts) {
        HashMap<Product, Integer> products = new HashMap<>();
        for (Product p : requestedProducts) {
            if (products.containsKey(p)) {
                products.put(p, products.get(p) + 1);
            }
            else {
                products.put(p, 1);
            }
        }
        return products;
    }
}
