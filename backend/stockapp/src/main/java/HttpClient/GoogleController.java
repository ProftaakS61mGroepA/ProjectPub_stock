package HttpClient;

import DataTransferObject.GoogleUserInfo;
import Service.ConfigService;
import com.google.gson.Gson;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.IOException;
import java.net.URL;
import java.util.*;

@Stateless
public class GoogleController {

    @Inject
    private HttpClient httpClient;
    @Inject
    private ConfigService configService;

    private String userinfoUrl;

    public GoogleController() { }

    @PostConstruct
    public void load() {
        userinfoUrl = configService.getProperty("oauth.google.userinfourl");
    }

    public GoogleUserInfo getUserInfo(String authorizationHeader) {
        try {
            //Send request to Google
            URL url = new URL(userinfoUrl);
            Map<String, String> headers = new HashMap<>(1);
            headers.put("Authorization", authorizationHeader);

            //Convert json to returntype
            return new Gson().fromJson(httpClient.sendGet(url, headers), GoogleUserInfo.class);

        } catch (IOException e) {
            throw new NullPointerException(e.getMessage());
        }
    }
}
