package DAO;

import DAO.JPAImpl.ProductDaoJPA;
import Models.*;

import static com.github.stefanbirkner.fishbowl.Fishbowl.exceptionThrownBy;

import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class ProductDaoJPATest {

    @InjectMocks
    private ProductDaoJPA productDao;

    @Mock
    private EntityManager em;

    private List<Product> products;

    @Before
    public void setUp() throws Exception {
        //Some test products
        products = new ArrayList<Product>() {{
            add(new Product("Cola", 200, Category.DRINK));
            add(new Product("Sprite", 200, Category.DRINK));
            add(new Product("Hamburger", 500, Category.FOOD));
        }};
        for (Product p : products) {
            p.setId(products.indexOf(p));
        }

        //Mock entitymanager with products list
        Mockito.when(em.find(Product.class, products.get(0).getId()))
                .thenReturn(products.get(0));
        final Query mockQuery = Mockito.mock(Query.class);
        final String query = "select t from Product t";
        Mockito.when(mockQuery.getResultList())
                .thenReturn(products);
        Mockito.when(em.createQuery(query))
                .thenReturn(mockQuery);
    }

    @Test
    public void createProductTest() throws Exception {
        final Product product = new Product("Cola", 10, Category.DRINK);
        Assert.assertEquals(product, productDao.create(product));

        //Test if empty product name is accepted
        Assert.assertNotNull(exceptionThrownBy(() -> productDao.create(new Product("", 10, Category.DRINK))));

        //Test if null product name is accepted
        Assert.assertNotNull(exceptionThrownBy(() -> productDao.create(new Product(null, 10, Category.DRINK))));

        //Test if negative price is accepted
        Assert.assertNotNull(exceptionThrownBy(() -> productDao.create(new Product("Cola", -1, Category.DRINK))));

        //Test if null category is accepted
        Assert.assertNotNull(exceptionThrownBy(() -> productDao.create(new Product("Cola", 10, null))));
    }

    @Test
    public void getAllProductsTest() throws Exception {
        final List<Product> newProducts = productDao.getAll();
        Assert.assertEquals(products.size(), newProducts.size());
    }

    @Test
    public void findByIdProductTest() throws Exception {
        Assert.assertEquals(products.get(0), productDao.findById(products.get(0).getId()));
    }

    @Test
    public void removeProductTest() throws Exception {
        productDao.remove(products.get(0));
        Mockito.verify(em)
                .remove(ArgumentMatchers.any());
        Mockito.verify(em)
                .merge(ArgumentMatchers.any());
    }
}
