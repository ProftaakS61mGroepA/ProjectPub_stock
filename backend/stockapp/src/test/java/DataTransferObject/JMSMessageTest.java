package DataTransferObject;

import org.junit.*;

public class JMSMessageTest {

    @Test
    public void jmsMessageJSONTest() throws Exception {
        //Create new dummy message
        JMSMessage oldMessage = new JMSMessage(0, 1, "Cola", 100);

        //Convert message to json
        String json = oldMessage.toJson();

        //Create new message from json
        JMSMessage newMessage = JMSMessage.fromJson(json);

        //Check if the messages are the same
        Assert.assertEquals(oldMessage.getId(), newMessage.getId());
        Assert.assertEquals(oldMessage.getProductId(), newMessage.getProductId());
        Assert.assertEquals(oldMessage.getProductName(), newMessage.getProductName());
        Assert.assertEquals(oldMessage.getAmount(), newMessage.getAmount());
    }
}
