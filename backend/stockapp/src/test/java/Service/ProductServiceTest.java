package Service;

import DAO.IProductDao;
import JMS.JMSBrokerGateway;
import Models.*;
import WebSocket.WebSocketSessionHandler;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

import static com.github.stefanbirkner.fishbowl.Fishbowl.exceptionThrownBy;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {

    @InjectMocks
    private ProductService productService;

    @Mock
    private IProductDao productDao;
    @Mock
    private JMSBrokerGateway jmsBrokerGateway;
    @Mock
    private WebSocketSessionHandler webSocketSessionHandler;

    private List<Product> products;

    @Before
    public void setUp() throws Exception {
        //Some test products
        products = new ArrayList();
        products.add(new Product("Cola", 200, Category.DRINK));
        products.add(new Product("Sprite", 200, Category.DRINK));
        products.add(new Product("Hamburger", 500, Category.FOOD));
        for (Product p : products) {
            p.setId(products.indexOf(p));
        }
        products.get(0).setReserved(1);
        products.get(1).setReserved(2);
        products.get(2).setInStock(0);

        //Mock productdao with products list

        Mockito.when(productDao.findById(products.get(0).getId()))
                .thenReturn(products.get(0));

        Mockito.when(productDao.findById(products.get(1).getId()))
                .thenReturn(products.get(1));

        Mockito.when(productDao.findById(products.get(2).getId()))
                .thenReturn(products.get(2));

        Mockito.when(productDao.getAll())
                .thenReturn(products);

        Mockito.when(productDao.edit(products.get(0)))
                .thenReturn(products.get(0));

        Mockito.when(productDao.edit(products.get(1)))
                .thenReturn(products.get(1));

        Mockito.doNothing()
                .when(jmsBrokerGateway).sendMessage(Mockito.anyLong(), Mockito.anyString(), Mockito.anyLong());
        Mockito.doNothing()
                .when(webSocketSessionHandler).sendToAll(Mockito.any(Product.class));
    }

    @Test
    public void createProductTest() throws Exception {
        final Product testProduct = new Product("Fries", 10, Category.FOOD);
        Mockito.when(productDao.create(testProduct))
                .thenReturn(testProduct);
        final Product newProduct = productService.create(testProduct);
        Assert.assertEquals("Created product not the same.",
                testProduct,
                newProduct);

        //Do not allow products with duplicate names
        final Product duplicateNameProduct = productService.create(products.get(0));
        Assert.assertEquals("Duplicate product was created.",
                null,
                duplicateNameProduct);
    }

    @Test
    public void getAllProductsTest() throws Exception {
        final List<Product> newProducts = productService.getAll();
        Assert.assertEquals("Did not return the correct amount of products.",
                products.size(),
                newProducts.size());
    }

    @Test
    public void findByIdProductTest() throws Exception {
        Assert.assertEquals("Did not return the right product.",
                products.get(0),
                productService.findById(products.get(0).getId()));
    }

    @Test
    public void removeProductTest() throws Exception {
        productService.removeProduct(products.get(0).getId());

        Mockito.verify(productDao)
                .remove(ArgumentMatchers.any());
    }

    @Test
    public void getUpdatedProductsTest() throws Exception {
        List<Product> requestedProducts = new ArrayList<>();
        requestedProducts.add(products.get(0));
        requestedProducts.add(products.get(1));
        Assert.assertEquals("Did not return the right products.",
                requestedProducts,
                productService.getUpdatedProducts(requestedProducts));
    }

    @Test
    public void reserveProductsTest() throws Exception {
        //Check if correct products are returned
        List<Product> requestedProducts = new ArrayList<>();
        requestedProducts.add(products.get(0));
        requestedProducts.add(products.get(1));
        final long availableBefore = requestedProducts.get(0).getAvailable();
        final long reservedBefore = requestedProducts.get(0).getReserved();
        final List<Product> returnedProducts = productService.reserveProducts(requestedProducts);
        Assert.assertEquals("Did not return the right products.",
                requestedProducts.size(),
                returnedProducts.size());

        //Check if available and reserved values are correctly set
        Product productAfter = null;
        for (Product p : returnedProducts) {
            if (p.getId() == requestedProducts.get(0).getId()) {
                productAfter = p;
                break;
            }
        }
        Assert.assertEquals("Available was not corrected.",
                availableBefore - 1,
                productAfter.getAvailable());
        Assert.assertEquals("Reserved was not corrected.",
                reservedBefore + 1,
                productAfter.getReserved());

        //Check if Exception is thrown when a product cannot be reserved.
        final List<Product> unreservableProducts = new ArrayList<>();
        unreservableProducts.add(products.get(2));
        Assert.assertNotNull("Did not thrown exception with unreservable product.",
                exceptionThrownBy(() -> productService.reserveProducts(unreservableProducts)));

        //Check if products reserved together with a unreservable product are not still reserved
        final long reservedBefore0 = products.get(0).getReserved();
        final long reservedBefore1 = products.get(1).getReserved();
        Assert.assertNotNull("Did not thrown exception with unreservable product.",
                exceptionThrownBy(() -> productService.reserveProducts(products)));
        Assert.assertEquals("Product was still reserved.",
                reservedBefore0, products.get(0).getReserved());
        Assert.assertEquals("Product was still reserved.",
                reservedBefore1, products.get(1).getReserved());
    }

    @Test
    public void useProductsTest() throws Exception {
        //Check if correct products are returned
        List<Product> requestedProducts = new ArrayList<>();
        requestedProducts.add(products.get(0));
        requestedProducts.add(products.get(1));
        requestedProducts.add(products.get(1));
        final long reservedBefore = products.get(1).getReserved();
        final long inStockBefore = products.get(1).getInStock();
        final List<Product> returnedProducts = productService.useProducts(requestedProducts);
        Assert.assertEquals("Did not return the right products.",
                requestedProducts.size(),
                returnedProducts.size());

        //Check if instock and reserved values are correctly set
        Product productAfter = null;
        for (Product p : returnedProducts) {
            if (p.getId() == requestedProducts.get(1).getId()) {
                productAfter = p;
                break;
            }
        }
        Assert.assertEquals("Reserved was not corrected.",
                reservedBefore - 2,
                productAfter.getReserved());
        Assert.assertEquals("InStock was not corrected.",
                inStockBefore - 2,
                productAfter.getInStock());

        //Check if Exception is thrown when a product cannot be used.
        final List<Product> unreservedProducts = new ArrayList<>();
        unreservedProducts.add(products.get(2));
        Assert.assertNotNull("Did not thrown exception with unreservable product.",
                exceptionThrownBy(() -> productService.reserveProducts(unreservedProducts)));

        //Check if products reserved together with a unreserved product are not used
        final long reservedBefore0 = products.get(0).getReserved();
        final long reservedBefore1 = products.get(1).getReserved();
        Assert.assertNotNull("Did not thrown exception with unreservable product.",
                exceptionThrownBy(() -> productService.reserveProducts(products)));
        Assert.assertEquals("Product was still reserved.",
                reservedBefore0, products.get(0).getReserved());
        Assert.assertEquals("Product was still reserved.",
                reservedBefore1, products.get(1).getReserved());
    }

    @Test
    public void orderProductTest() throws Exception {
        Product product = products.get(0);
        productService.orderProduct(product.getId(), 100);
        Mockito.verify(productDao).findById(product.getId());
        Mockito.verify(jmsBrokerGateway).sendMessage(product.getId(), product.getName(), 100);
    }

    @Test
    public void onOrderArrivedTest() throws Exception {
        productService.onOrderArrived(products.get(0).getId(), 100);
        Mockito.verify(productDao)
                .edit(Mockito.any(Product.class));
        Mockito.verify(webSocketSessionHandler)
                .sendToAll(Mockito.any(Product.class));
    }
}
