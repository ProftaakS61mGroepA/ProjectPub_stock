package Service;

import DAO.IAccountDao;
import Models.Account;

import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.URI;
import java.util.*;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {

    List<Account> accounts;

    @Mock
    private IAccountDao userDao;

    @Mock
    private OAuthService oAuthService;

    @InjectMocks
    private AccountService accountService;

    public AccountServiceTest() {
        accounts = new ArrayList<Account>() {{
            add(new Account("47312904"));
            add(new Account("3411235"));
            add(new Account("1261341"));
        }};
    }

    @Test
    public void requestLoginTest() throws Exception {
        final String URL = "www.google.nl";
        final URI URI = new URI(URL);
        Mockito.when(oAuthService.getAuthorizationUri(URL))
                .thenReturn(URI);

        URI response = accountService.requestLogin(URL);

        Assert.assertEquals("Did not return the right uri.",
                URI,
                response);
    }

    @Test
    public void findByGoogleIdTest() throws Exception {
        final Account testAccount = new Account("754221452");

        Mockito.when(userDao.findByGoogleId(testAccount.getGoogleId()))
                .thenReturn(testAccount);

        Assert.assertEquals("Did not return the right product.",
                testAccount,
                accountService.findByGoogleId(testAccount.getGoogleId()));
    }
}
