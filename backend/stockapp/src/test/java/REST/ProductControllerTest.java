package REST;

import DAO.JPAImpl.ProductDaoJPA;
import DataTransferObject.JsonProduct;
import Models.*;
import Service.ProductService;
import com.google.gson.*;
import org.glassfish.jersey.internal.inject.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.internal.stubbing.answers.CallsRealMethods;
import org.mockito.junit.MockitoJUnitRunner;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.*;

import java.util.*;

import static java.net.HttpURLConnection.*;

@RunWith(MockitoJUnitRunner.class)
public class ProductControllerTest extends JerseyTest {

    @Mock
    ProductDaoJPA dao;

    @InjectMocks
    private ProductService service;

    private List<Product> products;
    private List<JsonProduct> jsonProducts;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();

        //Some test products
        products = new ArrayList<Product>() {{
            add(new Product("Cola", 200, Category.DRINK));
            add(new Product("Sprite", 200, Category.DRINK));
            add(new Product("Hamburger", 500, Category.FOOD));
        }};
        for (Product p : products) {
            p.setId(products.indexOf(p));
        }
        products.get(0).setReserved(1);
        products.get(1).setReserved(2);
        products.get(2).setInStock(0);

        //Mock service with products list
        Mockito.when(service.getAll())
                .thenReturn(products);

        Mockito.when(service.findById(products.get(0).getId()))
                .thenReturn(products.get(0));

        Mockito.when(service.findById(products.get(1).getId()))
                .thenReturn(products.get(1));

        Mockito.when(service.findById(products.get(2).getId()))
                .thenReturn(products.get(2));

        Mockito.when(service.getUpdatedProducts(Mockito.anyList()))
                .thenCallRealMethod();

        Mockito.when(service.reserveProducts(Mockito.anyList()))
                .thenCallRealMethod();

        Mockito.when(service.useProducts(Mockito.anyList()))
                .thenCallRealMethod();

        Mockito.when(dao.edit(Mockito.any(Product.class)))
                .thenReturn(null);
        Mockito.doNothing()
                .when(service).orderProduct(1, 100);


        //Products to jsonProducts
        jsonProducts = JsonProduct.fromProducts(products);
    }

    @Override
    public Application configure() {
        service = Mockito.spy(ProductService.class);
        ResourceConfig config = new ResourceConfig(ProductController.class);
        config.register(new AbstractBinder() {
            @Override
            protected void configure() {
                bind(service).to(ProductService.class);
            }
        });
        return config;
    }

    @Test
    public void createProductTest() throws Exception {
        final Product testProduct = new Product("Sprite", 10, Category.DRINK);
        final JsonProduct testJsonProduct = JsonProduct.fromProduct(testProduct);
        Mockito.doReturn(testProduct).when(service).create(testProduct);

        //Try to create a new product called "Sprite"
        final Response correctResult = target("/products")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.json(testJsonProduct));

        //Check if the server returns the correct response
        Assert.assertEquals("Server did not return HTTP_CREATED.",
                HTTP_CREATED, correctResult.getStatus());
        //Check if the server creates the correct product
        Assert.assertEquals(testJsonProduct, correctResult.readEntity(JsonProduct.class));
        //Try to create a duplicate product
        final Response duplicateResult = target("/products")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.json(jsonProducts.get(0)));

        //Check if the server returns the correct response
        Assert.assertEquals("Server did not return HTTP_INTERNAL_ERROR.",
                HTTP_INTERNAL_ERROR, duplicateResult.getStatus());

        //Try to create a new empty product
        final Response faultyResult = target("/products")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.json(""));

        //Check if the server returns the correct response
        Assert.assertEquals("Server did not return HTTP_INTERNAL_ERROR.",
                HTTP_INTERNAL_ERROR, faultyResult.getStatus());

        //Try to create a new null product
        final Response faultyResult2 = target("/products")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.json(null));

        //Check if the server returns the correct response
        Assert.assertEquals("Server did not return HTTP_INTERNAL_ERROR.",
                HTTP_INTERNAL_ERROR, faultyResult2.getStatus());
    }

    @Test
    public void getAllProductsTest() throws Exception {
        final Response correctResult = target("/products")
                .request(MediaType.APPLICATION_JSON)
                .get();

        //Check if the server returns the correct response
        Assert.assertEquals("Server did not return HTTP_OK.",
                HTTP_OK, correctResult.getStatus());
        //Check if the server returns the correct products
        final List<JsonProduct> newProducts = correctResult.readEntity(new GenericType<List<JsonProduct>>(){});
        Assert.assertEquals(products.size(), newProducts.size());
    }

    @Test
    public void findByIdProductTest() throws Exception {
        final Response correctResult = target("/products/" + products.get(0).getId())
                .request(MediaType.APPLICATION_JSON)
                .get();

        //Check if the server returns the correct response
        Assert.assertEquals("Server did not return HTTP_OK.",
                HTTP_OK, correctResult.getStatus());
        //Check if the server returns the correct product
        final JsonProduct product = correctResult.readEntity(JsonProduct.class);
        Assert.assertEquals(products.get(0).getId(), product.getId());

        //Check if the right field are in the response
        final Response checkTransientResult = target("/products/" + products.get(0).getId())
                .request(MediaType.APPLICATION_JSON)
                .get();
        JsonObject obj = new JsonParser().parse(checkTransientResult.readEntity(String.class)).getAsJsonObject();
        Assert.assertNull("InStock should not be send over REST.", obj.get("inStock"));
        Assert.assertNull("Reserved should not be send over REST.", obj.get("reserved"));
        Assert.assertNotNull("Name should be send over REST.", obj.get("name"));
        Assert.assertNotNull("priceincents should be send over REST.", obj.get("priceInCents"));
        Assert.assertNotNull("category should be send over REST.", obj.get("category"));
        Assert.assertNotNull("available should be send over REST.", obj.get("available"));

        //Create a request to a non existing ID
        final Response wrongResult = target("/products/9999")
                .request(MediaType.APPLICATION_JSON)
                .get();

        //Check if the server returns the correct response
        Assert.assertEquals("Server did not return HTTP_INTERNAL_ERROR.",
                HTTP_INTERNAL_ERROR, wrongResult.getStatus());
    }

    @Test
    public void removeProductTest() throws Exception {
        final Response response = target("/products/" + jsonProducts.get(0).getId())
                .request()
                .delete();

        Assert.assertEquals("Server did not return HTTP_ACCEPTED.",
                HTTP_ACCEPTED, response.getStatus());
    }

    @Test
    public void getUpdatedProductsTest() throws Exception {
        List<JsonProduct> requestedProducts = new ArrayList<>();
        requestedProducts.add(jsonProducts.get(0));
        requestedProducts.add(jsonProducts.get(1));

        final Response response = target("/products/updated")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.json(requestedProducts));

        Assert.assertEquals("Server did not return HTTP_OK.",
                HTTP_OK, response.getStatus());
        Assert.assertEquals(requestedProducts, response.readEntity(new GenericType<List<JsonProduct>>() {}));
    }

    @Test
    public void reserveProductsTest() throws Exception {
        List<JsonProduct> requestedProducts = new ArrayList<>();
        requestedProducts.add(jsonProducts.get(0));
        requestedProducts.add(jsonProducts.get(1));

        final Response response = target("/products/reserve")
                .request(MediaType.APPLICATION_JSON)
                .put(Entity.json(requestedProducts));

        Assert.assertEquals("Server did not return HTTP_OK.",
                HTTP_OK, response.getStatus());
        Assert.assertEquals("Server did not return the same amount of products.",
                requestedProducts.size(),
                response.readEntity(new GenericType<List<JsonProduct>>() {}).size());

        List<JsonProduct> faultyProduct = new ArrayList<>();
        faultyProduct.add(jsonProducts.get(2));

        final Response faultyResponse = target("/products/reserve")
                .request(MediaType.APPLICATION_JSON)
                .put(Entity.json(faultyProduct));

        Assert.assertEquals("Server did not return HTTP_INTERNAL_ERROR.",
                HTTP_INTERNAL_ERROR, faultyResponse.getStatus());
    }

    @Test
    public void useProductsTest() throws Exception {
        List<JsonProduct> requestedProducts = new ArrayList<>();
        requestedProducts.add(jsonProducts.get(0));
        requestedProducts.add(jsonProducts.get(1));

        final Response response = target("/products/use")
                .request(MediaType.APPLICATION_JSON)
                .put(Entity.json(requestedProducts));

        Assert.assertEquals("Server did not return HTTP_OK.",
                HTTP_OK, response.getStatus());
        Assert.assertEquals("Server did not return the same amount of products.",
                requestedProducts.size(),
                response.readEntity(new GenericType<List<JsonProduct>>() {}).size());

        List<JsonProduct> faultyProduct = new ArrayList<>();
        faultyProduct.add(jsonProducts.get(2));

        final Response faultyResponse = target("/products/use")
                .request(MediaType.APPLICATION_JSON)
                .put(Entity.json(faultyProduct));

        Assert.assertEquals("Server did not return HTTP_INTERNAL_ERROR.",
                HTTP_INTERNAL_ERROR, faultyResponse.getStatus());
    }

    @Test
    public void orderProductTest() throws Exception {
        final long[] orderInfo = { 1, 100 };
        final Response response = target("/products/order")
                .request(MediaType.APPLICATION_JSON)
                .put(Entity.json(orderInfo));

        Assert.assertEquals("Server did not return HTTP_NO_CONTENT",
                HTTP_NO_CONTENT, response.getStatus());
    }
}
