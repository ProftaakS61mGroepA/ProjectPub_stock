package REST;

import DataTransferObject.AuthorizationInfo;
import DataTransferObject.JsonAccount;
import Models.Account;
import Service.AccountService;
import org.glassfish.jersey.internal.inject.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.*;
import java.net.*;

import static java.net.HttpURLConnection.HTTP_OK;

@RunWith(MockitoJUnitRunner.class)
public class AccountControllerTest extends JerseyTest {

    @InjectMocks
    private AccountController controller;

    @Mock
    private AccountService service;

    @Override
    protected Application configure() {
        final ResourceConfig config = new ResourceConfig(AccountController.class);
        config.register(new AbstractBinder() {
            @Override
            protected void configure() {
                bind(service).to(AccountService.class);
            }
        });
        return config;
    }

//    @Test
//    public void requestLoginAccountTest() throws Exception {
//        URI uri = new URI("www.google.nl");
//        Mockito.when(service.requestLogin())
//                .thenReturn(uri);
//
//        //Integration testing not necessary since method forwards the user to a external url
//        Response response = controller.requestLogin();
//
//        Assert.assertEquals("Login did not give the correct response code.",
//                HttpURLConnection.HTTP_SEE_OTHER,
//                response.getStatus());
//        Assert.assertEquals("Login did not give the correct uri.",
//                uri,
//                response.getLocation());
//    }

    @Test
    public void requestLoginAccountTest() throws Exception {
        final String URL = "www.google.nl";
        final URI URI = new URI(URL);
        Mockito.when(service.requestLogin(URL))
                .thenReturn(URI);

        final Response response = target("/accounts/login")
                .request(MediaType.TEXT_PLAIN)
                .post(Entity.text(URL));

        Assert.assertEquals("Login did not give the correct response code.",
                HttpURLConnection.HTTP_OK,
                response.getStatus());
        Assert.assertEquals("Login did not give the correct uri.",
                URL,
                response.readEntity(String.class));
    }

    @Test
    public void accountLoggedinTest() throws Exception {
        final String ACCESS_TOKEN = "zeihgbvauiad";
        Account account = new Account("32155423");
        JsonAccount jsonAccount = JsonAccount.fromAccount(account);
        jsonAccount.setAccessToken(ACCESS_TOKEN);
        Mockito.when(service.accountAuthorized(Mockito.anyString(), Mockito.anyString()))
                .thenReturn(account);
        Mockito.when(service.getAccessToken(Mockito.anyLong()))
                .thenReturn(ACCESS_TOKEN);

        final Response response = target("/accounts/postlogin")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.json(new AuthorizationInfo("authorizationCode", "www.redirectUrl.com")));

        Assert.assertEquals("Server did not return HTTP_OK",
                HTTP_OK, response.getStatus());
        Assert.assertEquals("Server did not return the right account.",
                jsonAccount, response.readEntity(JsonAccount.class));
    }
}
