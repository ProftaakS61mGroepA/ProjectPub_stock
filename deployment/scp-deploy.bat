set target=192.168.27.121
putty -ssh -pw student student@%target% -m "%~dp0/ssh-commands.txt"
pscp -r -pw student "%~dp0../gui/ProjectPub-stock/dist" student@%target%:git/ProjectPub/download/stock-frontend-build/
pscp -r -pw student "%~dp0../gui/ProjectPub-stock/httpd.conf" student@%target%:git/ProjectPub/download/stock-frontend-build/
pscp -r -pw student "%~dp0../gui/ProjectPub-stock/dockerfile" student@%target%:git/ProjectPub/download/stock-frontend-build/
pscp -r -pw student "%~dp0../backend/stockapp/target/*.war" student@%target%:git/ProjectPub/download/stock-backend-build/
