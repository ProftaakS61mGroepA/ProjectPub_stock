export const environment = {
  production: true,
  product_api: ':10021/stock/products',
  account_api: ':10021/stock/accounts',
  notification_endpoint: ':10021/stock/order-websocket',
  login_url: ':10020/login'
};
