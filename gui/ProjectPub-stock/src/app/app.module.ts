import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NgZone } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule, MatButtonModule, MatInputModule, MatSelectModule } from '@angular/material';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { CreateProductComponent } from './components/create-product/create-product.component';
import { ProductsComponent } from './components/products/products.component';
import { ProductService } from './services/product.service';
import { ConfigService } from './services/config.service';
import { ProductFilter } from './pipes/ProductFilter.pipe';
import { AccountService } from './services/account.service';
import { LoginComponent } from './components/login/login.component';
import { WebsocketService } from './services/websocket.service';
import { SocialLoginModule, AuthServiceConfig } from 'angularx-social-login';
import { GoogleLoginProvider, FacebookLoginProvider } from 'angularx-social-login';

const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider('97602268520-1881oa4ep2qt45vgsl08m9cjs0g46s6n.apps.googleusercontent.com')
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider('337699056712135')
  }
]);

export function provideConfig() {
  return config;
}

const appRoutes: Routes = [
  { path: 'products', component: ProductsComponent, pathMatch: 'full' },
  { path: 'login', component: LoginComponent, pathMatch: 'prefix' },
  { path: '', component: LoginComponent, pathMatch: 'full'}
];

@NgModule({
  declarations: [
    AppComponent,
    CreateProductComponent,
    ProductsComponent,
    ProductFilter,
    LoginComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatSelectModule,
    SocialLoginModule
  ],
  providers: [
    AccountService,
    ProductService,
    WebsocketService,
    ConfigService,
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
