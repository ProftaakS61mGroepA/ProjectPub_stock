import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { ResponseOptions } from '@angular/http';

import { ConfigService } from './config.service';
import { AuthorizationInfo } from '../models/authorizationInfo';
import { Account } from '../models/account';

@Injectable()
export class AccountService {

  constructor(
    private http: HttpClient,
    private config: ConfigService
  ) { }

  public requestLogin(): Observable<string> {
    return this.http.post(this.config.getAccountApi() + '/login', this.config.getLoginUrl(), {responseType: 'text'});
  }

  public accountLoggedin(authorizationCode: string): Observable<Account> {
    const authorizationInfo = new AuthorizationInfo(authorizationCode, this.config.getLoginUrl());
    return this.http.post<Account>(this.config.getAccountApi() + '/postlogin', authorizationInfo);
  }
}
