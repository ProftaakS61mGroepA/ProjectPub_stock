import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable()
export class ConfigService {
  private data = environment;

  private getUrl(): string {
    return window.location.protocol + '//' + window.location.hostname;
  }

  getLoginUrl(): string {
    return this.getUrl() + this.data.login_url;
  }

  getProductApi(): string {
    return this.getUrl() + this.data.product_api;
  }

  getAccountApi(): string {
    return this.getUrl() + this.data.account_api;
  }

  getWebsocketAddress(): string {
    return 'ws://' + window.location.hostname + this.data.notification_endpoint;
  }
}
