import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { AccountService } from './account.service';
import { ConfigService } from './config.service';
import { MockConfigService } from './config.service.mock';
import { Account } from '../models/account';

describe('AccountService', () => {
  let httpMock: HttpTestingController;
  let accountService: AccountService;
  let configMock: ConfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        { provide: ConfigService, useClass: MockConfigService },
        AccountService
      ]
    }).compileComponents();
    accountService = TestBed.get(AccountService);
    httpMock = TestBed.get(HttpTestingController);
    configMock = TestBed.get(ConfigService);
  });

  it('should be created', inject([AccountService], (service: AccountService) => {
    expect(service).toBeTruthy();
  }));
  it('should use the config mock', () => {
    expect(configMock.getAccountApi()).toBe('accounts');
    expect(configMock.getLoginUrl()).toBe('login');
  });
  it('requestLogin() should return a url', () => {
    const testUrl = 'www.google.com';

    accountService.requestLogin().subscribe((url) => {
      expect(url).toBe(testUrl);
    });

    const request = httpMock.expectOne('accounts/login');
    request.flush(testUrl);
    httpMock.verify();
  });
  it('accountLoggedin() should return an account', () => {
    const authorizationCode = 'authorizationCode';
    const expectedAccount = new Account(1, 'googleId', 'accessToken');

    accountService.accountLoggedin(authorizationCode).subscribe((account) => {
      expect(account.id).toBeGreaterThan(0);
      expect(account.googleId).toBe(expectedAccount.googleId);
      expect(account.accessToken).toBe(expectedAccount.accessToken);
    });

    const request = httpMock.expectOne('accounts/postlogin');
    request.flush({id: expectedAccount.id, googleId: expectedAccount.googleId, accessToken: expectedAccount.accessToken});
    httpMock.verify();
  });
});
