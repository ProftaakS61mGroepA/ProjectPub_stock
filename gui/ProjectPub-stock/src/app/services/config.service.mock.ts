import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';

@Injectable()
export class MockConfigService extends ConfigService {

    getLoginUrl(): string {
        return 'login';
    }

    getProductApi(): string {
        return 'products';
    }

    getAccountApi(): string {
        return 'accounts';
    }
}
