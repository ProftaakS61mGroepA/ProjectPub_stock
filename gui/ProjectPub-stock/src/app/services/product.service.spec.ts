import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ProductService } from './product.service';
import { ConfigService } from './config.service';
import { MockConfigService } from './config.service.mock';
import { Product } from '../models/product';


describe('ProductService', () => {
  let httpMock: HttpTestingController;
  let productService: ProductService;
  let configMock: ConfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        { provide: ConfigService, useClass: MockConfigService },
        ProductService
      ]
    }).compileComponents();

    productService = TestBed.get(ProductService);
    httpMock = TestBed.get(HttpTestingController);
    configMock = TestBed.get(ConfigService);
  });

  it('should be created', inject([ProductService], (service: ProductService) => {
    expect(service).toBeTruthy();
  }));
  it('should use the config mock', () => {
    expect(configMock.getProductApi()).toBe('products');
  });
  it('create() should return a Product', () => {
    const testProduct = new Product('Cola', 100, 'DRINK');

    productService.create(testProduct).subscribe((product) => {
      expect(product.id).toBeGreaterThan(0);
      expect(product.name).toBe(testProduct.name);
      expect(product.priceInCents).toBe(testProduct.priceInCents);
      expect(product.category).toBe(testProduct.category);
    });

    const request = httpMock.expectOne('products');
    request.flush({ id: 1, name: 'Cola', priceInCents: 100, category: 'DRINK' });
    httpMock.verify();
  });
  it('getAll() should return expected products', () => {
    const expectedProducts = [
      new Product('Cola', 100, 'DRINK', 0),
      new Product('Hamburger', 200, 'FOOD', 1),
    ];
    productService.getAll().subscribe((products) => {
      expect(products).toBe(expectedProducts);
    });
    const request = httpMock.expectOne('products');
    request.flush(expectedProducts);
    httpMock.verify();
  });
});
