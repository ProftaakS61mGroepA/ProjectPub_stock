import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { ResponseOptions } from '@angular/http';

import { ConfigService } from './config.service';
import { Product } from '../models/product';

@Injectable()
export class ProductService {

  constructor(
    private http: HttpClient,
    private config: ConfigService
) { }

  public create(product: Product): Observable<Product> {
    return this.http.post<Product>(this.config.getProductApi(), product);
  }

  public getAll(): Observable<Product[]> {
    return this.http.get<Product[]>(this.config.getProductApi());
  }

  public remove(productId: number): Observable<Object> {
    return this.http.delete(this.config.getProductApi() + '/' + productId);
  }

  public order(productId: number, amount: number) {
    this.http.put(this.config.getProductApi() + '/order', [productId, amount]).subscribe();
  }
}
