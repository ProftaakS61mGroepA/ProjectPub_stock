import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { Observer, Observable, Subject } from 'rxjs/Rx';

@Injectable()
export class WebsocketService {

  constructor(private config: ConfigService) { }
    private subject: Subject<MessageEvent>;
    public connect(): Subject<MessageEvent> {
      if (!this.subject) {
        this.subject = this.create();
      }
      return this.subject;
    }
    private create(): Subject<MessageEvent> {
      const url = this.config.getWebsocketAddress();
      const ws = new WebSocket(url);
      const observable = Observable.create(
        (obs: Observer<MessageEvent>) => {
          ws.onmessage = obs.next.bind(obs);
          ws.onerror = obs.error.bind(obs);
          ws.onclose = obs.complete.bind(obs);
          return ws.close.bind(ws);
        });
        const observer = {
        next: (data: Object) => {
          if (ws.readyState === WebSocket.OPEN) {
            ws.send(JSON.stringify(data));
          }
        }
      };
      return Subject.create(observer, observable);
    }
}
