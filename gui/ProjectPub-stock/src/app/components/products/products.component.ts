import { Component, OnInit, NgZone } from '@angular/core';
import { CurrencyPipe } from '@angular/common';

import { ProductService } from '../../services/product.service';
import { Product } from '../../models/product';
import { CreateProductComponent } from '../create-product/create-product.component';
import { WebsocketService } from '../../services/websocket.service';
import { AuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-products',
  providers: [ProductService],
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  creatingProduct = false;
  products: Product[] = [];
  public productFilter: Product = new Product();
  public categories: string[] = [
    'ALL',
    'FOOD',
    'DRINK',
    'OTHER'
  ];
  public user: SocialUser;
  public loggedIn: boolean;

  constructor(private productService: ProductService,
    private _zone: NgZone,
    private wsService: WebsocketService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.productFilter.category = this.categories[0];
    this.loadProducts();
    this.wsService.connect().subscribe(message => {
      const data = JSON.parse(message.data);
      this.products.find(product => product.id === data.id).available = data.available;
    });
    this.authService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = (user != null);
      if (!this.loggedIn) {
        this.router.navigate(['/login']);
      }
    });
  }
  newProductBtn(event) {
    this.creatingProduct = !this.creatingProduct;
  }
  onProductCreate(created: boolean) {
    this.creatingProduct = false;
    this.loadProducts();
  }
  deleteProductBtn(id: number) {
    this.productService.remove(id).subscribe(() => this.loadProducts());
  }
  orderProductBtn(id: number) {
    this.productService.order(id, 100);
  }
  private loadProducts() {
    this.productService.getAll().subscribe(products => {
      this._zone.run(() => this.products = products);
    });
  }

  logout() {
    this.authService.signOut();
  }
}
