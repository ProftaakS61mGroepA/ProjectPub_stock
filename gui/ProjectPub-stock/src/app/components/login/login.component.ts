import { Component, OnInit, NgZone } from '@angular/core';
import { AccountService } from '../../services/account.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FacebookLoginProvider, GoogleLoginProvider } from 'angularx-social-login';
import { AuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';

@Component({
  selector: 'app-login',
  providers: [AccountService],
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  authorizationCode: string;

  constructor(private accountService: AccountService,
    private _zone: NgZone,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService
  ) { }

  private user: SocialUser;
  private loggedIn: boolean;

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.authorizationCode = params['code'] || '';
    });

      if (this.authorizationCode !== '') {
        this.accountService.accountLoggedin(this.authorizationCode).subscribe(accounnt => {
          // ToDo store account
          this.router.navigate(['/products']);
        });
      }
      this.authService.authState.subscribe((user) => {
        this.user = user;
        this.loggedIn = (user != null);
        if (this.loggedIn) {
          this.router.navigate(['/products']);
        }
      });
  }
  loginBtn() {
    this.accountService.requestLogin().subscribe(url => window.location.href = url);
  }

  loginGoogle() {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  loginFacebook() {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }
}
