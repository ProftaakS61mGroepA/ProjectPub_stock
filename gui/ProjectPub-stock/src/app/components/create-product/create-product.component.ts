import { Component, OnInit, NgZone, Output, EventEmitter } from '@angular/core';

import { Product } from '../../models/product';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-create-product',
  providers: [ProductService],
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})
export class CreateProductComponent implements OnInit {

  public categories: string[] = [
    'FOOD',
    'DRINK',
    'OTHER'
  ];
  @Output() onProductCreate = new EventEmitter<boolean>();
  public newProduct: Product = new Product();
  public tempPrice = 0;

  constructor(private productService: ProductService,
    private _zone: NgZone
  ) { }

  ngOnInit() {
    this.newProduct.category = this.categories[0];
  }

  tempPriceChange(newPrice: number) {
    this.newProduct.priceInCents = newPrice * 100;
  }

  createProductBtn(prod: Product) {
    this.productService.create(prod).subscribe(product => {
      this._zone.run(() => {
        this.newProduct = new Product();
        this.newProduct.category = this.categories[0];
        this.tempPrice = 0;
        this.onProductCreate.emit(true);
      });
    });
  }

  isValidForm(): boolean {
    if (this.newProduct.name === null ||
      this.newProduct.name.trim() === '' ||
      this.newProduct.priceInCents < 0 ||
      this.newProduct.category.trim() === '') {
      return false;
    }
    return true;
  }
}
