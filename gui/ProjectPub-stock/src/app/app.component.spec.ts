import { TestBed, async } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule, MatButtonModule, MatInputModule, MatSelectModule } from '@angular/material';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterModule, Routes } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { APP_BASE_HREF } from '@angular/common';

import { AppComponent } from './app.component';
import { ProductsComponent } from './components/products/products.component';
import { CreateProductComponent } from './components/create-product/create-product.component';
import { ProductService } from './services/product.service';
import { ConfigService } from './services/config.service';
import { ProductFilter } from './pipes/ProductFilter.pipe';
import { LoginComponent } from './components/login/login.component';

const appRoutes: Routes = [
  { path: 'products', component: ProductsComponent, pathMatch: 'full' },
  { path: 'login', component: LoginComponent, pathMatch: 'prefix' },
  { path: '', component: LoginComponent, pathMatch: 'full'}
];

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        ProductsComponent,
        LoginComponent,
        CreateProductComponent,
        ProductFilter
      ],
      imports: [
        FormsModule,
        HttpClientTestingModule,
        BrowserAnimationsModule,
        MatCardModule,
        MatButtonModule,
        MatInputModule,
        MatSelectModule,
        RouterModule.forRoot(appRoutes, { enableTracing: true }),
        RouterTestingModule
      ],
      providers: [
        ConfigService,
        ProductService,
        { provide: APP_BASE_HREF, useValue : '/' }
      ]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('StockApp');
  }));
});
