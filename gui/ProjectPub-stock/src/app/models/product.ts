export class Product {
    constructor(
        public name: string = '',
        public priceInCents: number = 0,
        public category: string = '',
        public id: number = 0,
        public available: number = 0) { }
}
