export class AuthorizationInfo {
    constructor(
        public authorizationCode: string,
        public redirectUrl: string) { }
}
