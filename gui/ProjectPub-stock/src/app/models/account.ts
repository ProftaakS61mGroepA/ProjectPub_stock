export class Account {
    constructor(
        public id: number,
        public googleId: string,
        public accessToken: string) { }
}
