import { Pipe, PipeTransform } from '@angular/core';
import { Product } from '../models/product';

@Pipe({
    name: 'ProductFilter',
    pure: false
})
export class ProductFilter implements PipeTransform {
    transform(products: Product[], filter: Product): Product[] {
        if (!products || !filter) {
            return products;
        }
        return products.filter(product => this.applyFilter(product, filter));
    }

    applyFilter(product: Product, filter: Product): boolean {
        let success = true;
        if (filter.category !== 'ALL' && product.category !== filter.category) {
            success = false;
        }
        if (filter.name !== '' && product.name.toUpperCase().indexOf(filter.name.toUpperCase()) < 0) {
            success = false;
        }

        return success;
    }
}
